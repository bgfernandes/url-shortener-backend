const Url = require('./../models/Url');
const cache = require('./../cache/Cache').getCache();

module.exports = class UrlsController {
  // receives the shortUrl and longUrl and saves it
  static async createUrl(shortUrl, longUrl) {
    const url = new Url({
      shortUrl,
      longUrl,
    });

    // validate the model
    const error = await url.validate();
    if (error) {
      // check if it's a problem with one of the fields
      if (error.errors.shortUrl) {
        throw new Error(error.errors.shortUrl.message);
      } else if (error.errors.longUrl) {
        throw new Error(error.errors.longUrl.message);
      } else {
        throw new Error('Validation error.');
      }
    }

    // save the url
    try {
      await url.save();
    } catch (e) {
      if (e.code === 11000) {
        // this code means that the unique constraint was broken
        throw new Error('The short URL is already in use.');
      } else {
        // mask database error against malicious use
        throw new Error('Internal server error.');
      }
    }

    // return the complete shortUrl
    return `${process.env.BASEURL}/${shortUrl}`;
  }

  static async getByShortUrl(shortUrl) {
    // tries to find the shortUrl in the cache first
    let cacheItem;
    try {
      cacheItem = await cache.find(shortUrl);
      if (cacheItem) {
        // increment access counter, don't await for the return of this, fire and forget
        Url.update({ shortUrl }, { $inc: { accessCounter: 1 } }).exec();
        return cacheItem.longUrl;
      }
    } catch (e) {
      throw new Error('Internal server error.');
    }

    let url;
    try {
      // gets the URL by the shortUrl and updates the access counter
      url = await Url.findOneAndUpdate({ shortUrl }, { $inc: { accessCounter: 1 } }).select('longUrl');
    } catch (e) {
      // mask database error against malicious use
      throw new Error('Internal server error.');
    }

    if (!url) {
      const e = new Error('Short URL not found.');
      e.httpStatus = 404;
      throw e;
    }

    // put it in the cache for faster subsequent reads, don't await for return, fire and forget
    url.shortUrl = shortUrl;
    cache.add(url);

    return url.longUrl;
  }
};
