const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const helmet = require('helmet');

// open connection with mongodb
mongoose.connect(process.env.MONGO_CONNECTION_QUERY, { dbName: 'urlshortener' });

const app = express();

app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// status route
app.get('/status', (req, res) => {
  res.send('ok');
});

// application routes
const urlsRoutes = require('./routes/urls');

app.use('/urls', urlsRoutes);

// 404
app.all('*', (req, res) => {
  res.status(404).send({ success: false, message: 'Not found.' });
});

// eslint-disable-next-line max-params,no-unused-vars
app.use((err, req, res, next) => {
  console.error(err);
  res.status(500).send({ success: false, message: 'Internal server error.' });
});

module.exports = app;
