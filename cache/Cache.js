/*
This class currently implements a simple memory cache for speeding up reads.

This is not an optional solution for any but the simplest applications. With Node.JS, you would want
to run many instances of the server to optimize multi-core processor use. With this solution, each
instance would have their own different cache.

In a multi-instance installation, this memory cache should run in it's own instance.
In a multi-server installation, this cache should run in another machine, dedicated
for memory access.

An even better solution would be to have a cache with dedicated cache technology, like Redis.

This simple cache makes use of JS optimization on object lookups.

Although this cache could be accessed completely synchronously, I opted to use a Promise based
interface, so that in the future it could be upgraded to a asynchronous cache.

For cache replacement strategy, I will be removing the least recently used.
*/

const reduce = require('lodash/reduce');

const Url = require('./../models/Url');

let cacheInstance = null;

exports.getCache = () => {
  if (cacheInstance === null) {
    cacheInstance = new Cache({ maxSize: Number(process.env.CACHE_SIZE) });
  }
  return cacheInstance;
};

class Cache {
  constructor({ maxSize }) {
    if (typeof maxSize !== 'number') {
      throw new Error('Cache needs to receive a maxSize of type number on the constructor.');
    }

    if (maxSize < 1) {
      throw new Error('Invalid maxSize.');
    }

    this.maxSize = maxSize;
    this.currentSize = 0;
    this.initialized = false;
    this.data = {};
  }

  async init() {
    // this method is here for future development, in case there ever needs to be an
    // asynchronous initialization, like connecting to a remote cache server
    this.initialized = true;
  }

  async find(shortUrl) {
    if (!this.initialized) {
      throw new Error('Call init method before trying to use the Cache!');
    }

    if (typeof shortUrl !== 'string') {
      throw new Error('Invalid shortUrl parameter.');
    }

    if (!this.data[shortUrl]) {
      return null;
    }

    // update lastUsed
    this.data[shortUrl].lastUsed = (new Date()).getTime();

    return this.data[shortUrl];
  }

  async add(url) {
    if (!this.initialized) {
      throw new Error('Call init method before trying to use the Cache!');
    }

    if ((!(url instanceof Url)) || (url.validateSync())) {
      throw new Error('Invalid URL object.');
    }

    if (this.currentSize === this.maxSize) {
      await this._removeLeastRecentlyUsed();
    }

    this.data[url.shortUrl] = {
      longUrl: url.longUrl,
      lastUsed: (new Date()).getTime(),
    };

    this.currentSize += 1;
  }

  async _removeLeastRecentlyUsed() {
    let leastRecentlyUsedShortUrl = null;

    // combs stored items for the least recently used one
    reduce(this.data, (lowestTime, cacheItem, shortUrl) => {
      if (cacheItem.lastUsed < lowestTime) {
        leastRecentlyUsedShortUrl = shortUrl;
        return cacheItem.lastUsed;
      }
      return lowestTime;
    }, (new Date()).getTime());

    if (leastRecentlyUsedShortUrl) {
      delete this.data[leastRecentlyUsedShortUrl];
      this.currentSize -= 1;
    }
  }
}

exports.Cache = Cache;
