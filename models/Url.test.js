const Url = require('./Url');
const mongoose = require('mongoose');

require('dotenv').config();

beforeAll(() => {
  // establish connection to the test database
  mongoose.connect(process.env.MONGO_TEST_CONNECTION_QUERY, { dbName: 'test' });
});

afterEach(async () => {
  // delete all documents in the Url collection
  await Url.remove({});
});

it('should allow the creation of correct URLs', () => {
  let url = new Url({
    shortUrl: '4all',
    longUrl: 'https://www.4all.com',
  });

  expect(url.validateSync()).toBeUndefined();

  url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });

  expect(url.validateSync()).toBeUndefined();

  url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.test.com/a/page/inside.html',
  });

  expect(url.validateSync()).toBeUndefined();
});

it('should not allow the creation of a wrong type of URL', () => {
  let url = new Url({
    something: 'test',
    not: 123,
    allowed: ['a'],
  });

  expect(url.validateSync()).not.toBeUndefined();

  url = new Url({
    shortUrl: 'test',
  });

  expect(url.validateSync()).not.toBeUndefined();

  url = new Url({
    longUrl: 'test',
  });

  expect(url.validateSync()).not.toBeUndefined();

  url = new Url({
    shortUrl: 'test',
    longUrl: 'badly:/formed url',
  });

  expect(url.validateSync()).not.toBeUndefined();

  url = new Url({
    shortUrl: 'bad short',
    longUrl: 'https://www.google.com',
  });

  expect(url.validateSync()).not.toBeUndefined();
});

it('should save a URL correctly', async () => {
  const url = new Url({
    shortUrl: '4all',
    longUrl: 'https://www.4all.com',
  });

  await url.save();

  const sameUrl = await Url.findOne({ shortUrl: '4all' });

  expect(sameUrl.shortUrl).toBe('4all');
  expect(sameUrl.longUrl).toBe('https://www.4all.com');
});

it('should not allow saving two URLs with the same shortUrl', async () => {
  const url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });

  await url.save();

  const anotherUrl = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.anothergoogle.com',
  });

  let error;
  try {
    await anotherUrl.save();
  } catch (e) {
    error = e;
  }
  expect(error).not.toBeUndefined();
});

it('should not allow a URL withouth http:// or https://', () => {
  let url = new Url({
    shortUrl: 'google',
    longUrl: 'www.google.com',
  });

  expect(url.validateSync()).not.toBeUndefined();

  url = new Url({
    shortUrl: '4all',
    longUrl: '4all.com',
  });

  expect(url.validateSync()).not.toBeUndefined();
});

it('should set the accessCounter to 0 by default for a newly created URL', async () => {
  const url = new Url({
    shortUrl: '4all',
    longUrl: 'https://www.4all.com',
  });

  expect(url.accessCounter).toBe(0);

  await url.save();

  const sameUrl = await Url.findOne({ shortUrl: '4all' });

  expect(sameUrl.shortUrl).toBe('4all');
  expect(sameUrl.longUrl).toBe('https://www.4all.com');
  expect(sameUrl.accessCounter).toBe(0);
});

it('should incremente the access counter correctly', async () => {
  let url = new Url({
    shortUrl: '4all',
    longUrl: 'https://www.4all.com',
  });

  await url.save();

  url = await Url.findOne({ shortUrl: '4all' });

  expect(url.accessCounter).toBe(0);

  // increment with the document at hand
  url.accessCounter += 1;

  await url.save();

  url = await Url.findOne({ shortUrl: '4all' });

  expect(url.accessCounter).toBe(1);

  // increment without the document
  await Url.updateOne({ shortUrl: '4all' }, { $inc: { accessCounter: 1 } });

  url = await Url.findOne({ shortUrl: '4all' });

  expect(url.accessCounter).toBe(2);

  // increment and bring back the same document, after the update
  url = await Url.findOneAndUpdate({ shortUrl: '4all' }, { $inc: { accessCounter: 1 } }, { new: true });

  expect(url.accessCounter).toBe(3);
});
