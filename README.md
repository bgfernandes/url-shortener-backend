# URL Shortener Backend

This project was bootstrapped with [Express Generator](https://expressjs.com/en/starter/generator.html).

Run `npm install` to install depedencies.

Run `npm start` to start the server up.

Run `jest` to run the test cases.
