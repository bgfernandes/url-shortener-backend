// this middleware extracts the 'recaptchaResponse' from the payload and
// validates it
const fetch = require('node-fetch');
const { URLSearchParams } = require('url');

module.exports = (req, res, next) => {
  // check if the response is in the payload
  if (!req.body.recaptchaResponse) {
    res.status(403).send({ success: false, message: 'Complete captcha to make the request.' });
    return;
  }

  //
  const body = new URLSearchParams();
  body.append('secret', process.env.RECAPTCHA_SECRET_KEY);
  body.append('response', req.body.recaptchaResponse);

  fetch('https://www.google.com/recaptcha/api/siteverify', { method: 'POST', body })
    .then(fetchResponse => fetchResponse.json())
    .then((json) => {
      if (json.success !== true) {
        res.status(403).send({ success: false, message: 'Captcha validation failed.' });
        return;
      }

      // if everything is ok, remove the recaptchaResponse from the payload and call next middleware
      delete req.body.recaptchaResponse;
      next();
    })
    .catch(() => {
      res.status(500).send({ success: false, message: 'Captcha validation error.' });
    });
};
