const express = require('express');
const joi = require('joi');
const cors = require('cors');

const router = express.Router();

const postSchema = require('./../../schemas/urls/post');
const UrlsController = require('./../../controllers/UrlsController');
const recaptchaValidation = require('./../../middlewares/recaptchaValidation');

// for development, will allow CORS from http://localhost:3001
const corsOptions = {
  origin: 'http://localhost:3001',
};
router.use(cors(corsOptions));

// GET /urls/:shortUrl
// this route redirects the user to the long URL
router.get('/:shortUrl', (req, res) => {
  UrlsController.getByShortUrl(req.params.shortUrl)
    .then((longUrl) => {
      res.redirect(longUrl);
    })
    .catch((e) => {
      if (e.httpStatus) {
        res.status(e.httpStatus);
      } else {
        res.status(500);
      }
      res.send({ success: false, message: e.message });
    });
});

// POST /urls
// this route creates a shortened URL link
router.post('/', recaptchaValidation, (req, res) => {
  // schema validation
  const result = joi.validate(req.body, postSchema);
  if (result.error) {
    res.status(400).send({
      success: false,
      message: result.error.message,
    });
    return;
  }

  UrlsController.createUrl(req.body.shortUrl, req.body.longUrl)
    .then((finalShortUrl) => {
      res.send({ success: true, finalShortUrl });
    })
    .catch((e) => {
      res.status(500).send({ success: false, message: e.message });
    });
});


module.exports = router;
