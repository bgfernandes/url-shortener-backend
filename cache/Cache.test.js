const { Cache, getCache } = require('./Cache');

// I know I should mock this Model for unit testing, but for simplicity, I'll use it as is
const Url = require('./../models/Url');

// helper function, sleeps for m milliseconds
async function sleep(m) {
  return new Promise((resolve) => {
    setTimeout(resolve, m);
  });
}

test('getCache should return a Cache instance', () => {
  require('dotenv').config();
  const cache = getCache();
  expect(cache).toBeInstanceOf(Cache);
});

it('should create a cache with correct initial parameters', async () => {
  const cache = new Cache({ maxSize: 100 });

  expect(cache).toBeInstanceOf(Cache);
  expect(cache.maxSize).toBe(100);
  expect(cache.currentSize).toBe(0);
});

it('should throw if trying to create with bad initial parameters', () => {
  // eslint-disable-next-line no-unused-vars
  expect(() => { const cache = new Cache(); }).toThrow();

  // eslint-disable-next-line no-unused-vars
  expect(() => { const cache = new Cache({ maxSize: 0 }); }).toThrow();

  // eslint-disable-next-line no-unused-vars
  expect(() => { const cache = new Cache({ maxSize: -1 }); }).toThrow();

  // eslint-disable-next-line no-unused-vars
  expect(() => { const cache = new Cache({ maxSize: -100 }); }).toThrow();
});

it('should initialize without throwing an error', async () => {
  const cache = new Cache({ maxSize: 100 });

  let error;
  try {
    await cache.init();
  } catch (e) {
    error = e;
  }
  expect(error).not.toBeDefined();
});

it('should throw if trying to use the cache without initializing it first', async () => {
  const cache = new Cache({ maxSize: 100 });

  const url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });

  let error;
  try {
    await cache.add(url);
  } catch (e) {
    error = e;
  }
  expect(error).toBeDefined();
  expect(error.message).toBe('Call init method before trying to use the Cache!');

  error = undefined;
  try {
    await cache.find('google');
  } catch (e) {
    error = e;
  }
  expect(error).toBeDefined();
  expect(error.message).toBe('Call init method before trying to use the Cache!');
});

it('should add a new item to the cache correctly', async () => {
  const cache = new Cache({ maxSize: 100 });
  await cache.init();

  let url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });
  await cache.add(url);

  const googleCacheItem = await cache.find('google');
  expect(googleCacheItem).toBeDefined();
  expect(googleCacheItem).not.toBeNull();
  expect(googleCacheItem.longUrl).toBe('https://www.google.com');
  expect(typeof googleCacheItem.lastUsed).toBe('number');
  expect(cache.currentSize).toBe(1);

  await sleep(10);

  url = new Url({
    shortUrl: 'facebook',
    longUrl: 'https://www.facebook.com',
  });
  await cache.add(url);

  const facebookCacheItem = await cache.find('facebook');
  expect(facebookCacheItem).toBeDefined();
  expect(facebookCacheItem).not.toBeNull();
  expect(facebookCacheItem.longUrl).toBe('https://www.facebook.com');
  expect(typeof facebookCacheItem.lastUsed).toBe('number');
  expect(cache.currentSize).toBe(2);

  // expect that google is older than facebook on the cache
  expect(googleCacheItem.lastUsed).toBeLessThan(facebookCacheItem.lastUsed);
});

it('should not allow adding invalid urls to the cache', async () => {
  const cache = new Cache({ maxSize: 100 });
  await cache.init();

  const url = new Url({
    shortUrl: 'google',
    longUrl: 'not a valid url',
  });

  let error;
  try {
    await cache.add(url);
  } catch (e) {
    error = e;
  }
  expect(error).toBeDefined();
  expect(error.message).toBe('Invalid URL object.');

  error = undefined;
  try {
    await cache.add({ not: 'a url' });
  } catch (e) {
    error = e;
  }
  expect(error).toBeDefined();
  expect(error.message).toBe('Invalid URL object.');

  error = undefined;
  try {
    await cache.add();
  } catch (e) {
    error = e;
  }
  expect(error).toBeDefined();
  expect(error.message).toBe('Invalid URL object.');
});

it('should return null when trying to find an item that is not in the cache', async () => {
  const cache = new Cache({ maxSize: 100 });
  await cache.init();

  let cacheItem = await cache.find('google');
  expect(cacheItem).toBeNull();

  const url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });
  await cache.add(url);

  cacheItem = await cache.find('facebook');
  expect(cacheItem).toBeNull();
});

it('should update lastUsed when finding an item', async () => {
  const cache = new Cache({ maxSize: 100 });
  await cache.init();

  const url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });
  await cache.add(url);

  let cacheItem = await cache.find('google');
  let oldLastUsed = cacheItem.lastUsed;

  await sleep(10);

  cacheItem = await cache.find('google');
  expect(oldLastUsed).toBeLessThan(cacheItem.lastUsed);
  oldLastUsed = cacheItem.lastUsed;

  await sleep(10);

  cacheItem = await cache.find('google');
  expect(oldLastUsed).toBeLessThan(cacheItem.lastUsed);
  oldLastUsed = cacheItem.lastUsed;

  await sleep(10);

  cacheItem = await cache.find('google');
  expect(oldLastUsed).toBeLessThan(cacheItem.lastUsed);
  oldLastUsed = cacheItem.lastUsed;
});

it('should remove the least recently used item when adding to a full cache', async () => {
  const cache = new Cache({ maxSize: 2 });
  await cache.init();

  let url = new Url({
    shortUrl: 'google',
    longUrl: 'https://www.google.com',
  });
  await cache.add(url);

  await sleep(10);

  url = new Url({
    shortUrl: 'facebook',
    longUrl: 'https://www.facebook.com',
  });
  await cache.add(url);

  expect(cache.currentSize).toBe(2);

  // adding a new one should remove google
  url = new Url({
    shortUrl: '4all',
    longUrl: 'https://4all.com',
  });
  await cache.add(url);

  expect(cache.currentSize).toBe(2);

  let google = await cache.find('google');
  let facebook = await cache.find('facebook');
  let forAll = await cache.find('4all');

  expect(google).toBeNull();
  expect(facebook).not.toBeNull();
  expect(forAll).not.toBeNull();

  await sleep(10);

  await cache.find('facebook');

  expect(cache.currentSize).toBe(2);

  await sleep(10);

  // adding a new one should remove 4all
  url = new Url({
    shortUrl: 'twitter',
    longUrl: 'https://twitter.com',
  });
  await cache.add(url);

  expect(cache.currentSize).toBe(2);

  google = await cache.find('google');
  facebook = await cache.find('facebook');
  forAll = await cache.find('4all');
  let twitter = await cache.find('twitter');

  expect(google).toBeNull();
  expect(facebook).not.toBeNull();
  expect(forAll).toBeNull();
  expect(twitter).not.toBeNull();

  await sleep(10);

  await cache.find('facebook');

  await sleep(10);

  // adding a new one should remove twitter
  url = new Url({
    shortUrl: 'globo',
    longUrl: 'https://globo.com',
  });
  await cache.add(url);

  expect(cache.currentSize).toBe(2);

  google = await cache.find('google');
  facebook = await cache.find('facebook');
  forAll = await cache.find('4all');
  twitter = await cache.find('twitter');
  const globo = await cache.find('globo');

  expect(google).toBeNull();
  expect(facebook).not.toBeNull();
  expect(forAll).toBeNull();
  expect(twitter).toBeNull();
  expect(globo).not.toBeNull();
});
