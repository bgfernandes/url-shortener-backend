const Joi = require('joi');

module.exports = Joi.object().keys({
  shortUrl: Joi.string().required(),
  longUrl: Joi.string().required(),
});
