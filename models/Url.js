/*
This file holds the schema definition for the Url model

For development speed, ease of use, and performance constraints, I've decided
to use MongoDB and Mongoose, even though I'm not particularly used to working
with any of the two (this is my first time using Mongoose)
*/

const mongoose = require('mongoose');

const urlSchema = new mongoose.Schema({
  shortUrl: {
    type: String,
    required: true,
    match: [/^[a-zA-Z0-9]*$/, 'Short URL should have only numbers and letters.'],
    index: true,
    unique: true,
  },
  longUrl: {
    type: String,
    required: true,
    // regex taken from: https://www.regextester.com/94502
    // eslint-disable-next-line no-useless-escape
    match: [/^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/, 'Invalid long URL'],
  },
  accessCounter: {
    type: Number,
    default: 0,
    index: true,
  },
});

module.exports = mongoose.model('Url', urlSchema);
